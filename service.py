#!/usr/bin/env python

import threading
import time
import signal
import socket
import os
from gps import *

try:
    # so we can run this outside of kodi
    import xbmc
    import xbmcgui
    import xbmcaddon
    no_kodi=False
except ImportError:
    no_kodi=True

# This is so we can run it without kodi

try:
    __addon__       = xbmcaddon.Addon(id='script.service.gps')
    __addondir__    = xbmc.translatePath( __addon__.getAddonInfo('profile') )
except:
    __addondir__ = "/home/pi/.kodi/userdata/addon_data/script.service.gps/"

gps_poll=True

TCP_IP="0.0.0.0"
# picked the chipset of the radio
TCP_PORT=4703
BUFFER_SIZE=1024

variables={}

def log(logline):
    print "GPS: " + logline

def set_kodi_prop(property, value):
#    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)

    if no_kodi==True:
        variables[property]=value
        GPS.send_network(gps_kodi, "set_kodi_prop " +  property + " " + strvalue+"\n")
    else: 
        xbmcgui.Window(10000).setProperty(property, strvalue)

# Only used if doing the TCP IP stuff. I use this function (set_kodi_prop) too much
# to change the definition
def set_kodi_prop_lcl(property, value):
#    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)
    if no_kodi==True:
        variables[property]=value
    else: 
        xbmcgui.Window(10000).setProperty(property, strvalue)
        
def get_kodi_prop(property):
    if no_kodi==True:
        if property in variables:
            value=variables[property]
        else:
            value=""
            GPS.send_network(gps_kodi, "get_kodi_prop " +  property + "\n")
        ### Exception - because it's really annoying seeing once a second        
    else:
        value=xbmcgui.Window(10000).getProperty(property)
    return value        

def signal_handler(signal, frame):
    global gps_poll
    gps_poll=False
    
class GPS():
        
    #socket 
    sock=None
    gpsd=None
    
    def __init__(self):
        self.gpsd = gps(mode=WATCH_ENABLE) #starting the stream of info

    def start(self):
        global gps_poll
        
        if no_kodi==True:
            # we have to start the network first because of communication purposes.
            log ("Starting network")
            threading.Thread(target=self.start_network).start()
        
            # wait until the network is established
            while(self.sock==None):
                time.sleep(1)

        # This is just the GPS stuff
        while gps_poll==True:
            
            gps_info=self.gpsd.next()
#            print gps_info
            if "devices" in gps_info:
                if len(gps_info.devices)<1:
                    log("There are no GPS devices attached.")
                    time.sleep(10)
                    # get a new class and try again
                    self.gpsd = gps(mode=WATCH_ENABLE) #starting the stream of info
                elif "activated" not in gps_info.devices[0]:
                    log("There are no GPS devices attached.")
                    time.sleep(10)
                    # get a new class and try again
                    self.gpsd = gps(mode=WATCH_ENABLE) #starting the stream of info                    
                
            set_kodi_prop("gps_lat", str(self.gpsd.fix.latitude))
            set_kodi_prop("gps_lon", str(self.gpsd.fix.longitude))
            set_kodi_prop("gps_time", str(self.gpsd.utc))
            set_kodi_prop("gps_alt", str(self.gpsd.fix.altitude))
            set_kodi_prop("gps_eps", str(self.gpsd.fix.eps))
            set_kodi_prop("gps_epx", str(self.gpsd.fix.epx))
            set_kodi_prop("gps_epv", str(self.gpsd.fix.epv))
            set_kodi_prop("gps_ept", str(self.gpsd.fix.ept))
            set_kodi_prop("gps_speed", str(self.gpsd.fix.speed))
            set_kodi_prop("gps_climb", str(self.gpsd.fix.climb))
            set_kodi_prop("gps_track", str(self.gpsd.fix.track))
            set_kodi_prop("gps_mode", str(self.gpsd.fix.mode))
            time.sleep(1)
        exit(0)
            
    def network_response(self,data):
        data_split=data.split("\n")
        # loop through the data
        for data_line in data_split:
            request=data_line.split(" ")
            if request[0]=="set_kodi_prop":
                set_kodi_prop_lcl(request[1], " ".join(request[2:]))
        
    def start_network(self):
        # This is the network client handler
        # I decided to make this network enabled as well, since it can only run on the pi
        # and not easily be emulated for networking
        server_address = (TCP_IP, TCP_PORT)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect(server_address)

        while True:
            data = self.sock.recv(BUFFER_SIZE)
            if not data:
                log("Closing socket")
                self.sock.close()
                return
            self.network_response(data)

    def send_network(self, data):
        self.sock.send(data)
        
    def end(self):
        global gps_poll
        log("ENDING.")
        gps_poll=False
        
def kodi_mon():
    global gps_poll
    monitor = xbmc.Monitor()
    while True:
        if monitor.waitForAbort(1):
            GPS.end(gps_kodi)
            gps_poll=False
            os._exit(0)

if __name__ == "__main__":
    global gps_kodi

    log("Starting up")
    
    if no_kodi==False:
        threading.Thread(target=kodi_mon).start()
    else:
        signal.signal(signal.SIGINT, signal_handler)


    gps_kodi = GPS()
    gps_kodi.start()
